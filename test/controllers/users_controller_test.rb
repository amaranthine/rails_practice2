require 'test_helper'

class UsersControllerTest < ActionController::TestCase

  def setup
    @user = users(:michael) #admin is true on fixture
    @other_user = users(:archer)
  end


  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
    # assert_template 'users/index'
    # assert_response 200
  end

  test "should get new" do
    get :new
    assert_response :success
    assert_select "title", "Sign up | Ruby on Rails Tutorial Sample App"    
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end



  test "should redirect update when not logged in" do
  	#log_in_as()
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_user)
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert flash.empty?
    assert_redirected_to root_url
  end


  test "admin flag edit should failure" do
    log_in_as(@user)
    patch :update, id: @user, user: { name: @user.name, email: @user.email, admin: true }
    assert_not flash.empty?
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_url
  end

  test "should redirect to user_url destroy when logged in as a admin" do
    log_in_as(@user)
    assert @user.admin?
    assert_difference 'User.count', -1 do
      delete :destroy, id: @other_user
    end
    assert_redirected_to users_url
  end


  test "should redirect following when not logged in" do
    get :following, id: @user
    assert_redirected_to login_url
  end

  test "should redirect followers when not logged in" do
    get :followers, id: @user
    assert_redirected_to login_url
  end
  

end
