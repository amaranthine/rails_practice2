require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

	def setup
		@user = users(:michael) # from fixture
	end


	test "login with invalid information and flash movement confirmation" do
		get login_path
		assert_template 'sessions/new'
		assert_select "a[href=?]", new_password_reset_path
		post login_path, session: { email: "", password: "" }
		assert_template 'sessions/new'
		assert_not flash.empty?
		get root_path
		assert flash.empty?
	end


	test "login with valid information" do
		get login_path
		post login_path, session: { email: @user.email, password: "password" }
		assert is_logged_in?
		assert_redirected_to @user
		follow_redirect! #実際にそのページに移動
		assert_template 'users/show'
		assert_select "a[href=?]", login_path, count: 0
		assert_select "a[href=?]", logout_path
		assert_select "a[href=?]", user_path(@user)
		assert is_logged_in?

		delete logout_path
		assert_not is_logged_in?
		assert_redirected_to root_url
	    # 2番目のウィンドウでログアウトをクリックするユーザーをシミュレートする
	    delete logout_path
		follow_redirect!
		assert_select "a[href=?]", login_path
		assert_select "a[href=?]", logout_path,      count: 0
		assert_select "a[href=?]", user_path(@user), count: 0	

	end

	  test "login with remembering" do
	    log_in_as(@user, remember_me: '1')
	    assert_not_nil cookies['remember_token']
	  end

	  test "login without remembering" do
	    log_in_as(@user, remember_me: '0')
	    assert_nil cookies['remember_token']
	  end

end
