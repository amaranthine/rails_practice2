class SessionsController < ApplicationController

  def new
  	@user = User.new
  end


  def create

    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
     #  # ユーザーログイン
     #  log_in user

     #  # #永続化
     #  # remember user

	    # params[:session][:remember_me] == '1' ? remember(user) : forget(user)

     #  #ユーザー情報のページにリダイレクトする
     #  # redirect_to user # = user_url(user)
     #  redirect_back_or user
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end

    else
      # エラーメッセージを作成する
      user = User.new
      flash.now[:danger] = 'Invalid Login'
      render 'new'
    end

  end


  def destroy
    log_out if logged_in?
    redirect_to root_url
  end



end
