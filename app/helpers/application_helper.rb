module ApplicationHelper

	def full_title(each_title = '')

			@base_title = 'Ruby on Rails Tutorial Sample App'

			if each_title.empty?
				return @base_title
			else 
				return each_title + ' | ' + @base_title
			end

	end

end
